package springboot_druid_demo.service;


import springboot_druid_demo.model.User;

public interface UserService {

    int deleteByPrimaryKey(Integer var1);

    User insertSelective(User var1);

    User selectByPrimaryKey(Integer var1);

}
